# thesis workdir

setup correct python version
```
cd
git clone git://github.com/yyuu/pyenv.git .pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
source ~/.bashrc
```
reload bash
```
source ~/.bashrc
```
to see which environment are currently installed
```
pyenv global
```
and
```
pyenv versions
```
install python 3.6.3
```
pyenv install 3.6.3
```
check installed version
```
pyenv versions
```
set python 3.6.3 as the global environment to use
```
pyenv global 3.6.3
```
install pyenv-virtualenv and reload bash
```
git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
source ~/.bashrc
```
create venv 
```
mkdir virtual_env
cd virtual_env/
pyenv virtualenv 3.4.0 venv
```
list versions and environments 
```
pyenv versions
```
activate specific virtualenv
```
pyenv activate venv
```
pull requirements for python
```
pip install -r requirements.txt
```
if you need to update requirements use
```
pip freeze > requirements.txt
```